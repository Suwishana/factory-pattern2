package bcas.ap.dp.factory2;

public class SmallCar extends Car {
	private CarColour colour;
	public SmallCar (CarColour colour) {
		super(CarType.SMALL);
		this.colour =colour;
		assembel();
		paint();
	}
	@Override
	void assembel() {
		System.out.println("Assemble Small Car");
		
	}
	@Override
	void paint() {
		System.out.println("painted colour is"+colour);
	}

}



